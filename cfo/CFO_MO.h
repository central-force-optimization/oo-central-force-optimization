/* This file is part of CFO_MO in C++.

    CFO_MO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO_MO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CFO_MO in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#ifndef CFO_MO_H_
#define CFO_MO_H_

#include <algorithm>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>

#include "CFO.h"

#ifdef _WIN32
    #include <limits>
#endif

class CFO_MO : public CFO {

    public:
        CFO_MO();
        CFO_MO(int _Np, int _Nd, int _Nt,
                double _Alpha, double _Beta, double _Gamma,
                double _Frep, double _deltaFrep, std::vector<double> min, std::vector<double> max,
                std::vector<double>(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType = UNIFORM_ON_AXIS,
                bool _useAccelClipping = false, double _aMax = 1);
        CFO_MO(int _Np, int _Nd, int _Nt,
                double _Alpha, double _Beta, double _Gamma,
                double _Frep, double _deltaFrep, double min, double max,
                std::vector<double>(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType = UNIFORM_ON_AXIS,
                bool _useAccelClipping = false, double _aMax = 1);
        virtual ~CFO_MO();
                
        virtual double run();
        
        std::vector <int> getBestProbeNumberArray();
        std::vector < std::vector < double > > getBestFitnessArray();
               
        void setObjectiveFunction(std::vector<double>(*of)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int));
        void printResults();
        virtual void printDelimitedResults(std::string del = " ");

    protected:
        virtual void updateAcceleration(int j);
        bool hasFitnessSaturated(int nSteps, int j);
        void init(int _Np, int _Nd, int _Nt,
                double _Alpha, double _Beta, double _Gamma,
                double _Frep, double _deltaFrep, std::vector<double> _xMin, std::vector<double> _xMax,
                std::vector<double>(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType,
                bool _useAccelClipping, double _aMax);


        virtual void evalFitness(int j);
        virtual void reset();

        std::vector<double>(*objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int);             
        std::vector<double> bestFitness, worstFitness;
        std::vector< std::vector < double > > bestFitnessArray;
        std::vector<std::vector < std::vector < double > > > M;

        std::string functionName;
        CStopWatch timer, timer1;
};

#endif /* CFO_MO_H_ */
