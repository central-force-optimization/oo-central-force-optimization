/* This file is part of CFO in C++.

CFO in C++ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

CFO in C++ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 Nicholas Rodgers
*/

#include "CFO_Multiplicity.h"
#include "MultiplicityProbe.h"

double CFO_Multiplicity::accuracy = 1E-3;

CFO_Multiplicity::CFO_Multiplicity() {
	cfoType = MULTIPLICITY;
}

CFO_Multiplicity::CFO_Multiplicity(int _Np, int _Nd, int _Nt,
	double _Alpha, double _Beta, double _Gamma,
	double _Frep, double _deltaFrep, std::vector<double> min, std::vector<double> max,
	double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>&, int, int, int),
	std::string _functionName,
	IPD_TYPE _ipdType,
	bool _useAccelClipping, double _aMax) {

	init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, min, max, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
	cfoType = MULTIPLICITY;
}

CFO_Multiplicity::CFO_Multiplicity(int _Np, int _Nd, int _Nt,
	double _Alpha, double _Beta, double _Gamma,
	double _Frep, double _deltaFrep, double min, double max,
	double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>&, int, int, int),
	std::string _functionName,
	IPD_TYPE _ipdType,
	bool _useAccelClipping, double _aMax) {

	std::vector < double > _xMin(_Np, min);
	std::vector < double > _xMax(_Np, max);
	init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, _xMin, _xMax, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);

	cfoType = MULTIPLICITY;
}

void CFO_Multiplicity::updatePositions(int j) {
	combineFactors(j);

	timer.startTimer();
	for (int p = 0; p < Np; p++) {
		if (std::dynamic_pointer_cast<MultiplicityProbe>(Probes[j - 1][p])->MultiplicityFactor == 0)
			continue;

		for (int i = 0; i < Nd; i++) {
			Probes[j][p]->Position[i] = Probes[j - 1][p]->Position[i] + Probes[j - 1][p]->Acceleration[i];
		}
	}
	timer.stopTimer();
	positionTime += timer.getElapsedTime();
}
void CFO_Multiplicity::calculateInfluence(int p, int i, int k, int j) {

}

void CFO_Multiplicity::evalFitness(int j) {
	timer.startTimer();
	for (int p = 0; p < Np; p++) {
		if (std::dynamic_pointer_cast<MultiplicityProbe>(Probes[j][p])->MultiplicityFactor == 0)
			continue;
		Probes[j][p]->Fitness = objFunc(Probes, Nd, p, j);

		numEvals++;

		if (Probes[j][p]->Fitness >= bestFitness) {
			bestFitness = Probes[j][p]->Fitness;
			bestTimeStep = j;
			bestProbeNumber = p;

			bestFitnessArray[j] = Probes[j][p]->Fitness;
			bestProbeNumberArray[j] = p;
		}
		if (Probes[j][p]->Fitness < worstFitness) {
			worstFitness = Probes[j][p]->Fitness;
			worstTimeStep = j;
			worstProbeNumber = p;
		}

	}
	timer.stopTimer();
	fitnessTime += timer.getElapsedTime();
}

void CFO_Multiplicity::combineFactors(int j) {
	std::shared_ptr<MultiplicityProbe> currProbe;
	std::shared_ptr<MultiplicityProbe> otherProbe;

	double sumSQ;

	for (int p = 0; p < Np; p++) {
		currProbe = std::dynamic_pointer_cast<MultiplicityProbe>(Probes[j - 1][p]);

		if (currProbe->MultiplicityFactor == 0)
			continue;

		for (int k = p + 1; k < Np; k++) {
			otherProbe = std::dynamic_pointer_cast<MultiplicityProbe>(Probes[j - 1][k]);
			if (otherProbe->MultiplicityFactor == 0)
				continue;

			sumSQ = 0;
			for (int i = 0; i < Nd; i++)
			{
				sumSQ += pow(otherProbe->Position[i] - currProbe->Position[i], 2);
			}

			if (sumSQ < CFO_Multiplicity::accuracy)
			{
				if (currProbe->Fitness >= otherProbe->Fitness)
				{
					currProbe->MultiplicityFactor += otherProbe->MultiplicityFactor;
					otherProbe->MultiplicityFactor = 0;
				}
				else
				{
					otherProbe->MultiplicityFactor += currProbe->MultiplicityFactor;
					currProbe->MultiplicityFactor = 0;
				}
			}
		}

	}
}
void CFO_Multiplicity::updateAcceleration(int j) {
	timer.startTimer();
	int p, i, k;
	double sumSQ, denom, numerator, deltaMass;
	double alpha = Alpha;
	double beta = Beta;

#pragma omp parallel default(none) private(sumSQ, denom, numerator, p, i, k, deltaMass) firstprivate(j, alpha, beta)
	{
#pragma omp for schedule(dynamic) 
		for (p = 0; p < Np; p++) {
			if (std::dynamic_pointer_cast<MultiplicityProbe>(Probes[j][p])->MultiplicityFactor == 0)
				continue;
			for (i = 0; i < Nd; i++) {
				Probes[j][p]->Acceleration[i] = 0;
				for (k = 0; k < Np; k++) {
					if (k != p) {
						if (std::dynamic_pointer_cast<MultiplicityProbe>(Probes[j][k])->MultiplicityFactor == 0)
							continue;
						deltaMass = Probes[j][k]->Fitness - Probes[j][p]->Fitness;
						if (deltaMass > 0)
						{
							sumSQ = 0.0;

							for (int L = 0; L < Nd; L++) {
								sumSQ = sumSQ + pow(Probes[j][k]->Position[L] - Probes[j][p]->Position[L], 2);
							}
							if (sumSQ != 0) {
								denom = sqrt(sumSQ);
								numerator = unitStep(deltaMass) * (deltaMass);
								Probes[j][p]->Acceleration[i] += (Probes[j][k]->Position[i] - Probes[j][p]->Position[i]) * pow(numerator, alpha) / pow(denom, beta);
							}
						}
					}
				}
			}
		}
	}
	timer.stopTimer();
	accelTime += timer.getElapsedTime();
}

bool CFO_Multiplicity::hasConverged(int j)
{
	timer.startTimer();
	// Convergence occurs when 20% of probes are within accuracy neighborhood
	std::shared_ptr<Probe> bestProbe = Probes[j][bestProbeNumber];
	int inNeighborhood = std::dynamic_pointer_cast<MultiplicityProbe>(bestProbe)->MultiplicityFactor;

	for (auto& p : Probes[j])
	{
		if (p != bestProbe)
		{
			double distance = 0;
			for (int i = 0; i < Nd; i++)
			{
				distance += pow(p->Position[i] - bestProbe->Position[i], 2);
			}
			if (pow(distance, 0.5) < CFO_Multiplicity::accuracy)
			{
				inNeighborhood += std::dynamic_pointer_cast<MultiplicityProbe>(p)->MultiplicityFactor;
			}
		}
	}
	timer.stopTimer();
	convergeTime += timer.getElapsedTime();
	return (inNeighborhood >= (0.2 * Np));
}

void CFO_Multiplicity::reset() {

	bestTimeStep = 0;  worstTimeStep = 0;
	bestProbeNumber = 0;  worstProbeNumber = 0;
	numProbesPerAxis = Np / Nd;
	if (numProbesPerAxis == 0) { numProbesPerAxis = 1; }

	numEvals = 0;
	numCorrections = 0;

#ifdef _WIN32
	bestFitness = -std::numeric_limits<double>::infinity();
	worstFitness = std::numeric_limits<double>::infinity();
#else
	bestFitness = -INFINITY;
	worstFitness = -INFINITY;
#endif
	positionTime = 0, correctionTime = 0,
		fitnessTime = 0, accelTime = 0,
		shrinkTime = 0, convergeTime = 0,
		totalTime = 0;

	bestProbeNumberArray.clear();
	bestFitnessArray.clear();
	diversity.clear();

	Probes.resize(Nt);
	for (auto& probes : Probes) {
		probes.clear();
		probes.reserve(Np);
		for (int i = 0; Np != i; i++) {
			probes.push_back(std::make_unique<MultiplicityProbe>(Nd));
		}
	}

	bestProbeNumberArray.resize(Nt, 0);
	bestFitnessArray.resize(Nt, 0);

	xMin = originalXMin;
	xMax = originalXMax;
}