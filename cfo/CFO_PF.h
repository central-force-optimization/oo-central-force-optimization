/* This file is part of CFO in C++.

    CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#ifndef CFO_PF_H_
#define CFO_PF_H_

#include "CFO.h"
#include "CFO_Binary.h"
#include "CFO_Global_Best.h"
#include "CFO_Global_Best_Worst.h"
#include "CFO_Linear.h"
#include "CFO_Mesh.h"
#include "CFO_Ring.h"
#include "CFO_Star.h"
#include "CFO_Static_Tree.h"
#include "CFO_Toroidal.h"
#include "CFO_Multiplicity.h"

class CFO_PF {

    public:
        CFO_PF();
        CFO_PF(int _Nd, int _Nt,
            double _Alpha, double _Beta, double _Gamma,
            double _Frep, double _deltaFrep, std::vector<double> min, std::vector<double> max,
            double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
            std::string _functionName,
            IPD_TYPE _ipdType = UNIFORM_ON_AXIS,
            bool _useAccelClipping = false, double _aMax = 1);

        CFO_PF(int _Nd, int _Nt,
                double _Alpha, double _Beta, double _Gamma,
                double _Frep, double _deltaFrep, double min, double max,
                double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType = UNIFORM_ON_AXIS,
                bool _useAccelClipping = false, double _aMax = 1);
        virtual ~CFO_PF();
        
        int getBestNp();
        int getBestProbeNumber();
        int getBestTimeStep();
        int getLastStep();
        int getBestNumEvals();
        int getTotalNumEvals();
        int getBestNumCorrections();
        
        double getBestGamma();
        double getBestFitness();
        double getPositiontime();
        double getCorrectionTime();
        double getfitnessTime();
        double getAccelTime();
        double getShrinkTime();
        double getConvergeTime();
        double run();
        
        void setCFO(CFO* cfo);
        void setCfoType(CFO_TYPE ct);
        void setNd(int d);
        void setNt(int d);
        void setUseAccelClipping(bool b);
        void setAMax(double d);
        void setAlpha(double a);
        void setBeta(double b);
        void setGamma(double g);
        void setFrep(double fr);
        void setDeltaFrep(double dfr);
        void setXMin(double min);
        void setXMax(double max);
        void setIPDType(IPD_TYPE _ipdType);
        void setXMin(std::vector<double> min);
        void setXMax(std::vector<double> max);
        void setFunctionName(std::string fname);
        void setPrintDelimitedResults(bool b);
        void setPrintFancyResults(bool b);
        void setLogToFile(bool ltf);
        void setObjectiveFunction(double(*of)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int));
        
        void printResults();
        void printDelimitedResults(std::string del = " ");

    private:
        std::string ipdString();
        void calculateMaxProbes();
        void init(int _Nd, int _Nt,
                double _Alpha, double _Beta, double _Gamma,
                double _Frep, double _deltaFrep,
                std::vector<double> min, std::vector<double> max,
                double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType,
                bool _useAccelClipping, double _aMax);
        void reset();
        
        IPD_TYPE ipdType;
        bool useAccelClipping, _printDelimitedResults, _printFancyResults, logTofile;
        int Np, Nd, Nt, numProbesPerAxis, maxProbesPerDimension, bestLastStep;
        int bestNp, bestNt, bestNpNd;
        int bestTimeStep, bestProbeNumber;
        int bestNumEvals, totalEvals;
        int bestNumCorrections;

        double aMax;
        double Alpha, Beta, Gamma, numGammas, bestGamma;
        double Frep, deltaFrep;
        double bestFitness;
        double positionTime, correctionTime, 
                    fitnessTime,  accelTime, 
                    shrinkTime,   convergeTime,
                    totalTime;

        double(*objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int);

        std::vector <double> xMin, xMax;

        std::string functionName;
        CStopWatch timer, timer1;

        CFO_TYPE cfoType;

        CFO* C;
};

#endif /* CFO_H_ */
