#ifndef PROBE_H_
#define PROBE_H_

#include <vector>
#include <limits>

class Probe {
public:
	Probe(int Nd);
	std::vector<double> Acceleration;
	std::vector<double> Position;
	double Fitness;
	virtual ~Probe();
};
#endif
