/* This file is part of CFO in C++.

    CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#include "CFO_PF.h"
CFO_PF::CFO_PF(){
    std::vector < double > _xMin(0, 0);
    std::vector < double > _xMax(0, 0);
    init(0, 0, 0, 0, 0, 0, 0, _xMin, _xMax, NULL, "", UNIFORM_ON_AXIS, false, 0.0);
}

CFO_PF::CFO_PF(int _Nd, int _Nt,
        double _Alpha, double _Beta, double _Gamma,
        double _Frep, double _deltaFrep, std::vector<double> min, std::vector<double> max,
        double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
        std::string _functionName,
        IPD_TYPE _ipdType,
        bool _useAccelClipping, double _aMax) {
        
    init(_Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, min, max, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
}
CFO_PF::CFO_PF(int _Nd, int _Nt,
        double _Alpha, double _Beta, double _Gamma,
        double _Frep, double _deltaFrep, double min, double max,
        double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
        std::string _functionName,
        IPD_TYPE _ipdType,
        bool _useAccelClipping, double _aMax) {
        
    std::vector < double > _xMin(_Nd, min);
    std::vector < double > _xMax(_Nd, max);
    init(_Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, _xMin, _xMax, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
}

CFO_PF::~CFO_PF() {
    xMin.clear(); xMax.clear();    
    objFunc = NULL;
    delete C; C = NULL;
}

void CFO_PF::init(int _Nd, int _Nt,
        double _Alpha, double _Beta, double _Gamma,
        double _Frep, double _deltaFrep,
        std::vector<double> min, std::vector<double> max,
        double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
        std::string _functionName,
        IPD_TYPE _ipdType,
        bool _useAccelClipping, double _aMax) {
                
    Nd		= _Nd;		Nt = _Nt;
    Alpha	= _Alpha;	Beta = _Beta; Gamma = _Gamma;
    Frep	= _Frep;	deltaFrep = _deltaFrep;
    xMin	= min;		xMax = max;
    objFunc = _objFunc; functionName = _functionName;
    
    useAccelClipping = _useAccelClipping; 
    aMax			 = _aMax;
    ipdType			 = _ipdType;
    cfoType	         = STANDARD;
    C				 = NULL;

    _printDelimitedResults = false;
    _printFancyResults     = false;
}
void CFO_PF::calculateMaxProbes(){

    if(Nd >= 1 && Nd <= 6)		  { maxProbesPerDimension = 14;}
    else if(Nd >= 7 && Nd <= 10)  { maxProbesPerDimension = 12;}
    else if(Nd >= 11 && Nd <= 15) { maxProbesPerDimension = 10;}
    else if(Nd >= 16 && Nd <= 20) { maxProbesPerDimension = 8; }
    else if(Nd >= 21 && Nd <= 30) {	maxProbesPerDimension = 6; }
    else						  { maxProbesPerDimension = 4; }

}
void CFO_PF::reset(){
    calculateMaxProbes();
    numGammas			= 21;
    bestGamma			= 0;	bestNpNd		= 0; bestLastStep = 0;
    bestTimeStep		= 0;	bestProbeNumber = 0;
    bestNumEvals		= 0;	totalEvals		= 0;
    numProbesPerAxis	= Np/Nd;
    bestNumCorrections	= 0;

    #ifdef _WIN32
        bestFitness = -std::numeric_limits<double>::infinity();
    #else
        bestFitness  = -INFINITY;
    #endif
    positionTime = 0, correctionTime = 0, 
    fitnessTime  = 0, accelTime      = 0, 
    shrinkTime   = 0, convergeTime   = 0,
    totalTime    = 0;

    if(C != NULL){
        delete C;
        C = NULL;
    }
}

double CFO_PF::run(){
    timer.startTimer();
    reset();

    if(C == NULL){
        if(cfoType == STANDARD)				   { C = new CFO();}
        else if (cfoType == RING)			   { C = new CFO_Ring;}
        else if (cfoType == STAR)			   { C = new CFO_Star;}
        else if (cfoType == LINEAR)			   { C = new CFO_Linear;}
        else if (cfoType == MESH)			   { C = new CFO_Mesh;}
        else if (cfoType == TOROIDAL)		   { C = new CFO_Toroidal;}
        else if (cfoType == STATIC_TREE)	   { C = new CFO_Static_Tree;}
        else if (cfoType == BINARY)			   { C = new CFO_Binary;}
        else if (cfoType == GLOBAL_BEST)	   { C = new CFO_Global_Best;}
        else if (cfoType == GLOBAL_BESTWORST)  { C = new CFO_Global_Best_Worst;}
		else if (cfoType == MULTIPLICITY)      { C = new CFO_Multiplicity; }
    }	

    for(int numProbesPerAxis = 2; numProbesPerAxis <= maxProbesPerDimension; numProbesPerAxis += 2){
        Np = numProbesPerAxis*Nd;

        if(ipdType == UNIFORM_ON_AXIS){
            for(int GammaNumber=1; GammaNumber <= numGammas; GammaNumber++){
                Gamma = (GammaNumber-1)/(numGammas-1);
                
                C->setNp(Np);        C->setNd(Nd);	   C->setNt(Nt);
                C->setAlpha(Alpha);  C->setBeta(Beta); C->setGamma(Gamma);
                C->setXMin(xMin);    C->setXMax(xMax);
                C->setFrep(Frep);    C->setDeltaFrep(deltaFrep);
                C->setObjectiveFunction(objFunc); C->setFunctionName(functionName);
                C->setIPDType(ipdType);
                C->setUseAccelClipping(useAccelClipping); C->setAMax(aMax);
                C->setLogToFile(logTofile);
                C->run();
                if(_printDelimitedResults) { C->printDelimitedResults(); } 
                else if(_printFancyResults){C->printResults(); }
            
                positionTime    += C->getPositiontime();
                correctionTime  += C->getCorrectionTime();
                fitnessTime     += C->getFitnessTime();
                accelTime       += C->getAccelTime();
                shrinkTime      += C->getShrinkTime();
                convergeTime    += C->getConvergeTime();
            
                totalEvals += C->getNumEvals();
            
                if(C->getBestFitness() >= bestFitness){
                    bestFitness 		= C->getBestFitness();
                    bestProbeNumber		= C->getBestProbeNumber();
                    bestTimeStep 		= C->getBestTimeStep();
                    bestNpNd 			= numProbesPerAxis;
                    bestGamma 			= Gamma;
                    bestLastStep		= C->getLastStep();
                    bestNp				= Np;
                    bestNumEvals		= C->getNumEvals();
                    bestNumCorrections  = C->getNumCorrections();
                }
            }
        }else{
            C->setNp(Np);        C->setNd(Nd);	   C->setNt(Nt);
            C->setAlpha(Alpha);  C->setBeta(Beta); C->setGamma(Gamma);
            C->setXMin(xMin);    C->setXMax(xMax);
            C->setFrep(Frep);    C->setDeltaFrep(deltaFrep);
            C->setObjectiveFunction(objFunc); C->setFunctionName(functionName);
            C->setIPDType(ipdType);
            C->setUseAccelClipping(useAccelClipping); C->setAMax(aMax);
            C->run();
            if(_printDelimitedResults) { C->printDelimitedResults(); } 
            else if(_printFancyResults){ C->printResults(); }
            
            positionTime    += C->getPositiontime();
            correctionTime  += C->getCorrectionTime();
            fitnessTime     += C->getFitnessTime();
            accelTime       += C->getAccelTime();
            shrinkTime      += C->getShrinkTime();
            convergeTime    += C->getConvergeTime();
            
            totalEvals += C->getNumEvals();
            
            if(C->getBestFitness() >= bestFitness){
                bestFitness 		= C->getBestFitness();
                bestProbeNumber		= C->getBestProbeNumber();
                bestTimeStep 		= C->getBestTimeStep();
                bestNpNd 			= numProbesPerAxis;
                bestGamma 			= Gamma;
                bestLastStep		= C->getLastStep();
                bestNp				= Np;
                bestNumEvals		= C->getNumEvals();
                bestNumCorrections  = C->getNumCorrections();
            }
        }
    }
    
    timer.stopTimer();
    totalTime = timer.getElapsedTime();
    
    delete C; C = NULL;
    return bestFitness;
}

int CFO_PF::getBestNp()             { return bestNp;}
int CFO_PF::getBestTimeStep()       { return bestTimeStep;}
int CFO_PF::getBestProbeNumber()    { return bestProbeNumber;}
int CFO_PF::getLastStep()           { return bestLastStep; }
int CFO_PF::getBestNumEvals()       { return bestNumEvals; }
int CFO_PF::getTotalNumEvals()      { return totalEvals; }
int CFO_PF::getBestNumCorrections() { return bestNumCorrections;}
double CFO_PF::getBestGamma()       { return bestGamma;}
double CFO_PF::getBestFitness()     { return bestFitness;}
double CFO_PF::getPositiontime()    { return positionTime; }
double CFO_PF::getCorrectionTime()  { return correctionTime; }
double CFO_PF::getfitnessTime()     { return fitnessTime; }
double CFO_PF::getAccelTime()       { return accelTime; }
double CFO_PF::getShrinkTime()      { return shrinkTime; }
double CFO_PF::getConvergeTime()    { return convergeTime; }

void CFO_PF::setCfoType(CFO_TYPE ct)            { cfoType = ct;	}
void CFO_PF::setCFO(CFO* cfo)                   { C = cfo; }
void CFO_PF::setNd(int d)                       { Nd = d; }
void CFO_PF::setNt(int d)                       { Nt = d; }
void CFO_PF::setAMax(double d)                  { aMax = d; }
void CFO_PF::setAlpha(double a)                 { Alpha = a; }
void CFO_PF::setBeta(double b)                  { Beta = b;	 }
void CFO_PF::setGamma(double g)                 { Gamma = g; }
void CFO_PF::setFrep(double fr)                 { Frep = fr; }
void CFO_PF::setXMin(std::vector<double> min)   { xMin = min; }
void CFO_PF::setXMax(std::vector<double> max)   { xMax = max; }
void CFO_PF::setXMin(double min)                { xMin.clear();		xMin.resize(Nd, min); }
void CFO_PF::setXMax(double max)                { xMax.clear();		xMax.resize(Nd, max); }
void CFO_PF::setDeltaFrep(double dfr)           { deltaFrep = dfr; }
void CFO_PF::setUseAccelClipping(bool b)        { useAccelClipping = b; }
void CFO_PF::setLogToFile(bool ltf)             { logTofile = ltf; }
void CFO_PF::setIPDType(IPD_TYPE _ipdType)      { ipdType = _ipdType; }
void CFO_PF::setFunctionName(std::string fname) { functionName = fname; }
void CFO_PF::setPrintDelimitedResults(bool b)   { _printDelimitedResults = b; }
void CFO_PF::setPrintFancyResults(bool b)       { _printFancyResults = b; }

void CFO_PF::setObjectiveFunction(double(*of)(std::vector <std::vector<std::shared_ptr<Probe>>> & ,int, int, int)){ objFunc = of; }

void CFO_PF::printResults(){
    std::cout << "Function Name:  		"         << functionName				<< std::endl;
    std::cout << "Initial Probe Dist.   "		  << Utils::ipdString(ipdType)	<< std::endl;
    std::cout << "Best Overall Fitness: 		" << std::setprecision(18)		<< bestFitness		<< std::endl;
    std::cout << "Best Overall Probe #: 		" << std::setprecision(0)		<< bestProbeNumber	<< std::endl;
    std::cout << "Best Overall TimeStep:		" << bestTimeStep				<< std::endl;
    std::cout << "Best Gamma:		   	"         << bestGamma					<< std::endl;
    std::cout << "Best Number of Evals: 		" << bestNumEvals				<< std::endl;
    std::cout << "Total Number of Evals: 		" << totalEvals					<< std::endl;
    std::cout << "Best Probes Per Axis: 		" << bestNpNd					<< std::endl;
    std::cout << "Best Number Of Probes:		" << bestNp						<< std::endl;
    std::cout << "Overall Last Step:    		" << bestLastStep				<< std::endl;
}

void CFO_PF::printDelimitedResults(std::string del){
    std::cout   << functionName						<< del
                << Utils::cfoTypeString(cfoType)	<< del
                << Utils::ipdString(ipdType)		<< del
                << bestFitness						<< del 
                << bestProbeNumber					<< del
                << bestTimeStep						<< del
                << bestGamma						<< del
                << bestNpNd							<< del
                << bestNp							<< del
                << Nd								<< del
                << bestLastStep						<< del
                << bestNumEvals						<< del
                << totalTime                        << std::endl;

    if(logTofile){
        std::ofstream myFile;
        std::string fileName;
        
        fileName = "../Results/CFO_PF_" + functionName + "_";
        fileName += Utils::cfoTypeString(cfoType) + "_";
        fileName += Utils::ipdString(ipdType) + "_";
        fileName = Utils::changeBase(fileName, Nd);
        fileName = fileName + ".csv";
        myFile.open(fileName.c_str(), std::ios::app);
        if(myFile.is_open()){
            myFile	<< functionName						<< del
                    << Utils::cfoTypeString(cfoType)	<< del
                    << Utils::ipdString(ipdType)		<< del
                    << bestFitness						<< del 
                    << bestProbeNumber					<< del
                    << bestTimeStep						<< del
                    << bestGamma						<< del
                    << bestNpNd							<< del
                    << bestNp							<< del
                    << Nd								<< del
                    << bestLastStep						<< del
                    << bestNumEvals					<< del
                    << totalTime					<< std::endl << std::endl;
        }
        myFile.close();
    }
}
