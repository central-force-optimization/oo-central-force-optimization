/* This file is part of CFO in C++.

    CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#ifndef CFO_Extended_H_
#define CFO_Extended_H_

#include "CFO.h"

class CFO_Extended : public CFO{
    public:
        CFO_Extended();
        CFO_Extended(int _Np, int _Nd, int _Nt,
                double _Alpha, double _Beta, double _Gamma,
                double _Frep, double _deltaFrep, std::vector<double> min, std::vector<double> max,
                double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType = UNIFORM_ON_AXIS,
                bool _useAccelClipping = false, double _aMax = 1);

        CFO_Extended(int _Np, int _Nd, int _Nt,
                double _Alpha, double _Beta, double _Gamma,
                double _Frep, double _deltaFrep, double min, double max,
                double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
                std::string _functionName,
                IPD_TYPE _ipdType = UNIFORM_ON_AXIS,
                bool _useAccelClipping = false, double _aMax = 1);

        virtual ~CFO_Extended();

    private:
        double unitStep(double X);
        void updatePositions(int j);
        void updateAcceleration(int j);

        double St;
        double maxGr, minGr, eqGr, gR;
};

#endif