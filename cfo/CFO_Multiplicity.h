/* This file is part of CFO in C++.

CFO in C++ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

CFO in C++ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 Nicholas Rodgers
*/

#ifndef CFO_Multiplicity_H_
#define CFO_Multiplicity_H_

#include "CFO.h"

class CFO_Multiplicity : public CFO {

public:
	CFO_Multiplicity();
	CFO_Multiplicity(int _Np, int _Nd, int _Nt,
		double _Alpha, double _Beta, double _Gamma,
		double _Frep, double _deltaFrep, std::vector<double> min, std::vector<double> max,
		double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>&, int, int, int),
		std::string _functionName,
		IPD_TYPE _ipdType = UNIFORM_ON_AXIS,
		bool _useAccelClipping = false, double _aMax = 1);

	CFO_Multiplicity(int _Np, int _Nd, int _Nt,
		double _Alpha, double _Beta, double _Gamma,
		double _Frep, double _deltaFrep, double min, double max,
		double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>&, int, int, int),
		std::string _functionName,
		IPD_TYPE _ipdType = UNIFORM_ON_AXIS,
		bool _useAccelClipping = false, double _aMax = 1);
	void reset();
private:
	virtual void evalFitness(int j);
	virtual void updatePositions(int j);
	virtual void updateAcceleration(int j);
	virtual void calculateInfluence(int p, int i, int k, int j);
	virtual bool hasConverged(int j);
	static double accuracy;
protected:
	void combineFactors(int j);
};

#endif /* CFO_H_ */
