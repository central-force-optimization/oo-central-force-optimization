/* This file is part of CFO in C++.

    CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#include "CFO_Mesh.h"

CFO_Mesh::CFO_Mesh(){
    cfoType = MESH;
}

CFO_Mesh::CFO_Mesh(	int _Np, int _Nd, int _Nt,
            double _Alpha, double _Beta, double _Gamma,
            double _Frep, double _deltaFrep, std::vector<double> min, std::vector<double> max,
            double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
            std::string _functionName,
            IPD_TYPE _ipdType,
            bool _useAccelClipping, double _aMax){

    init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, min, max, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
    cfoType = MESH;
}

CFO_Mesh::CFO_Mesh(	int _Np, int _Nd, int _Nt,
            double _Alpha, double _Beta, double _Gamma,
            double _Frep, double _deltaFrep, double min, double max,
            double(*_objFunc)(std::vector <std::vector<std::shared_ptr<Probe>>>& ,int, int, int),
            std::string _functionName,
            IPD_TYPE _ipdType,
            bool _useAccelClipping , double _aMax) {

    std::vector < double > _xMin(_Np, min);
    std::vector < double > _xMax(_Np, max);
    init(_Np, _Nd, _Nt, _Alpha, _Beta, _Gamma, _Frep, _deltaFrep, _xMin, _xMax, _objFunc, _functionName, _ipdType, _useAccelClipping, _aMax);
    
    cfoType = MESH;
}
CFO_Mesh::~CFO_Mesh(){}

void CFO_Mesh::calculateInfluence(int p, int i, int k, int j){
    SumSQ = 0.0;
    for (int L = 0; L < Nd; L++) {
        SumSQ = SumSQ + pow(Probes[j][k]->Position[L] - Probes[j][p]->Position[L], 2);
    }
    if(SumSQ != 0){
        Denom = sqrt(SumSQ);
        Numerator = unitStep((Probes[j][k]->Fitness - Probes[j][p]->Fitness)) * (Probes[j][k]->Fitness- Probes[j][p]->Fitness);
        Probes[j][p]->Acceleration[i] = Probes[j][p]->Acceleration[i] + (Probes[j][k]->Position[i] - Probes[j][p]->Position[i]) * pow(Numerator,Alpha)/pow(Denom,Beta);
    }
}
void CFO_Mesh::updateAcceleration(int j){
    timer.startTimer();
    int north, south, east, west;
    int numCols, numRows;

    numRows = 4; numCols = Np/numRows; 
    for (int p = 0; p < Np; p++) {
        
        if(p >= numCols)		{ north = p - numCols;}
        else					{ north = -1;}
        if(p < Np-numCols)		{ south  = p + numCols;}
        else					{ south = -1;}
        if((p+1) % numCols != 0){ east  = p + 1;} 
        else					{ east  = -1;}
        if(p % numCols != 0)	{ west  = p - 1;}
        else					{ west  = -1;}
        
        for(int i = 0; i < Nd; i++) {
            Probes[j][p]->Acceleration[i] = 0; // Zero Out Acceleration
            if(north != -1) { calculateInfluence(p, i, north, j);}
            if(south != -1) { calculateInfluence(p, i, south, j);}
            if(east  != -1) { calculateInfluence(p, i, east, j);}
            if(west  != -1) { calculateInfluence(p, i, west, j);}
        }
    }
    timer.stopTimer();
    accelTime += timer.getElapsedTime();
}
