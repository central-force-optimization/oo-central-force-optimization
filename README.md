## Synopsis

Central Force Optimization (CFO) is a new and deterministic population based  metaheuristic  algorithm  that  has  been  demonstrated  to  be  competitive  with other metaheuristic algorithms such as Genetic Algorithms (GA), Particle Swarm Optimization (PSO), and Group Search Optimization (GSO). While CFO often shows superiority in terms of functional evaluations and solution quality, the algorithm is complex and typically requires increased computational time. In addition, CFO is completely deterministic, including no aspect of random behavior. This design feature has a significant impact on the algorithm, it's behavior, and it's performance.

Source: 
Central force optimization on a GPU: a case study in high performance metaheuristics
The Journal of Supercomputing
Volume 62 Issue 1, October 2012
Pages 378-398

## Command Line

 	--help				Prints this help
 	--threads			# of threads to use with OpenMP 
 	--Nd				# of dimensions 
 	--Nt             	# of test  
 	--alpha          	Value for alpha 
 	--beta           	Value for beta 
 	--threadPerProbe 	Fixes threads based on Np value 
 	--runPF          	Runs PF_CFO prior to running CFO

## Motivation

Metaheuristic  algorithms,  specifically  those  that  may  be  classified  as  population based intelligent search (PIS) or population-based metaheuristics (PBMs), have proven to be quite useful in solving a wide range of problems across many fields. These algorithms typically mimick some aspect of nature in order to solve hard computational problems. Many of these algorithms, like particle swarm optimization (PSO) or genetic algorithms (GAs) include some aspect of random variation. 

CFO, in contrast, is completely deterministic, resulting in fundamentally different behavior.


## Building 
The software currently can be built with make or Visual Studio. A Dockerfile is also included for ease of use. 

To build the software using make, make sure that g++ 4.8 or better, make, and sqlite are installed and then:

    cd Default && make all

To build the software using Docker on Linux, use the following commands:

    docker run -v $PWD:/code -it rgreen13/cfo
    cd /code/Default && make all
    ./CFO.o

To build the software using Docker on Windows:

    docker run -v ${PWD}:/code -it rgreen13/cfo
    cd /code/Default && make all
    ./CFO.o

## Adding Other CFO Algorithms

In order to add a new CFO variant, use the following steps:
1. Choose a name for your variant. The naming convention is "CFO_XXX".
2. Create CFO\_XXX.h and CFO\_XXX.cpp files under the "cfo" directory
3. Create a class of the same name ("CFO\_XXX") that inherits from "CFO"
4. Create the appropriate 3 Constructors (See code)
5. Create the appropriate Destructor (See code)
6. Overload the appropriate functions in the cpp file, typically `updateAcceleration(int j)` and `calculateInfluence(int p, int i, int k, int j);`

## Contributors

Robert C. Green II, Ph.D [website](https://rgreen13.gitlab.io), 
Nick Rodgers
Harrison Renny
