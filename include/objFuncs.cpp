#include "objFuncs.h"
#ifdef _WIN32
#include <algorithm>
#endif

namespace ObjFuncs{
	// Goldberg's order-3 problem:
	// Be sure dimension=3*k
	// Max value is Nd/3
	// Uses Nd/3 - f as Objective
	// The min value is 0  on 1111...111
	double binaryF1(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){ 
		double Z   = 0;
		double sum = 0;

		for(int d=0; d<Nd; d+=3){
			sum = 0;
			for(int i=d; i<d+3; i++){
				sum += Probes[j][p]->Position[i];
			}

			if(sum == 0){ Z += 0.9; continue;}
			if(sum == 1){ Z += 0.6; continue;}
			if(sum == 2){ Z += 0.3; continue;}
			if(sum == 3){ Z += 1.0; continue;}
		}
		return Z;
	}

	// Bipolar order-6
	// Min 0 on any position ..6x0..6x1..
	// Be sure dimension=6*k
	// Max value is Nd/6
	// Uses Nd/6 - f as Objective
	// The min value is 0  on 1111...111
	double binaryF2(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){ 
		double Z   = 0;
		double sum = 0;

		for(int d=0; d<Nd-3; d=d+6){
			sum=0;
			for(int i=d;i<d+6;i++){
				sum += Probes[j][p]->Position[i];
			}

			if (sum == 0 || sum == 6) { Z += 1.0; continue;}
			if (sum == 1 || sum == 5) { Z += 0.0; continue;}
			if (sum == 2 || sum == 4) { Z += 0.4; continue;}
			if (sum == 3)             { Z += 0.8; continue;}
			
		}

		return Z;
	}

	// Muhlenbein's order-5
	// Min 0 on any position ..5x0..5x1..
	// Be sure dimension=5*k
	// Max value is 3.5*Nd/5
	// Uses Nd/5 - f as Objective
	// The min value is 0  on 1111...111
	double binaryF3(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){ 
		double Z   = 0;
		std::string tempStr = "00000";

		for(int d=0; d<Nd-5; d=d+5){
			
            for(int i=d;i<d+5;i++){
				if(Probes[j][p]->Position[i] == 0) { tempStr[i%5] = '0';}
				else				{ tempStr[i%5] = '1';}
			}

			if(tempStr.compare("00000") == 0) { Z += 4.0; continue;}
			if(tempStr.compare("00001") == 0) { Z += 3.0; continue;}
			if(tempStr.compare("00011") == 0) { Z += 2.0; continue;}
			if(tempStr.compare("00111") == 0) { Z += 1.0; continue;}
			if(tempStr.compare("11111") == 0) { Z += 3.5; continue;}
		}
		return Z;
	}

	// Clerc's Zebra3
	// Be sure dimension=3*k
	// Max value is Nd/3
	// Uses Nd/3 - f as Objective
	// The min value is 0  on 111000111000...000111
	double binaryF4(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){ 
		double Z   = 0;
		double sum = 0;
		int num = 0;

		for(int d=0; d<Nd; d+=3){

			sum = 0;
			num++;
			for(int i=d; i<d+3; i++){
				sum += Probes[j][p]->Position[i];
			}
			if(num % 2 == 0){
				if(sum == 0){ Z += 0.9; continue;}
				if(sum == 1){ Z += 0.6; continue;}
				if(sum == 2){ Z += 0.3; continue;}
				if(sum == 3){ Z += 1.0; continue;}
			}else{
				if(sum == 0){ Z += 1.0; continue;}
				if(sum == 1){ Z += 0.3; continue;}
				if(sum == 2){ Z += 0.6; continue;}
				if(sum == 3){ Z += 0.9; continue;}
			}
		}
		return Z;
	}

    // Multi-Modal generated landscape
    // Min = 0
    double binaryF5(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){ 
        double Z, f1;
        MTRand mt;
        int peaks = 20;
        std::vector< std::vector<double> > landscape(peaks, std::vector<double>(Nd, 0.0));

        for(int i=0; i<peaks; i++){
            for(int j=0; j<Nd; j++){
	            landscape[i][j] = mt.rand();//rand() & 01;
            }
        }

        Z = 0;
        for (int j=0; j<peaks; j++){
	        f1 = 0.0;
	        for(int k=0; k<Nd; k++){
	           if((int)Probes[j][p]->Position[k] == landscape[j][k]){
                   f1++;
               }
	        }

	        if(f1 > Z){
                Z = f1; 
            }
       }
       return Z;
    }
    double binaryF11(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){ 
        double Z = 0.0;
        std::vector<int> hIdeal(121, 0);

        hIdeal[48] = 1.0; hIdeal[49] = 1.0; hIdeal[50] = 1.0;
        hIdeal[59] = 1.0; hIdeal[60] = 1.0; hIdeal[61] = 1.0;
        hIdeal[70] = 1.0; hIdeal[71] = 1.0; hIdeal[72] = 1.0; 

        for(int i=0; i<Nd; i++){
            Z += std::abs(hIdeal[i] - Probes[j][p]->Position[i]);
        }

        return -Z;
    }
	double decode(int start, int stop, std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int p, int j){
	   double sum, n;
	   int x;

	   sum 	= 0.0;
	   n 	= 1.0;

	   for (x = start; x < stop; x++) {
		   sum = sum + Probes[j][p]->Position[x]*n;
		   n = n * 2;
	   }
	   return sum;
	}

	/* 3 variables, 10 bits/variable. */
	// Sphere function
    //solution = 78.6;
	double binaryF6(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j) {
		double x1, x2, x3, Z=0;

		
		x1 = (decode(0 , 10, Probes, p, j) - 512) / 100.0;
		x2 = (decode(10, 20, Probes, p, j) - 512) / 100.0;
		x3 = (decode(20, Nd, Probes, p, j) - 512) / 100.0;
		Z += pow(x1,2);
		Z += pow(x2,2);
		Z += pow(x3,2);

		//cout << x1 << " " << x2 << " " << x3 << " " << Z << endl;
		return Z;
	}

	/* 2 variables, 12 bits/variable. */
    // Rosenbrock
    //solution = 3905.93;
	double binaryF7(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j) {
		double x1, x2, Z = 0;

		
		x1 = (double)(decode(0 , 12, Probes, p, j) - 2048) / 1000.0;
		x2 = (double)(decode(12 , 24, Probes, p, j) - 2048) / 1000.0;

		Z = ((100.0 * pow(pow(x1, 2.0) - x2, 2.0)) + pow(1.0 - x1, 2.0));
		return Z;
	};

	/* 5 variables, 10 bits/variable. */
    // Step Function
    //solution = 55.0;
	double binaryF8(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j) {
		int Z=0;
		double temp;

		
		for(int x=0; x<5; x++) {
			temp = (decode(x*10, (x*10)+10, Probes, p, j) - 512.0)/100.00;
            Z += floor(temp);
		}

		return Z;
	};

	/* 30 variables, 8 bits/variable. ssrand() needs to be defined to be
	   a (0,1) gaussian random variable */
	double binaryF9(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j) {
		register int x;
        MTRand mt;
		double Z;
		double temp;

		//solution = 1248.2;
		Z = 0.0;
		for (int x = 0; x < 30; x++) {
			temp = (decode(x*8, (x*8)+8, Probes, p, j)-128.0)/100;
			temp = pow(temp,4) + mt.rand();
			Z = Z + temp;
		}

		return -Z;
	}

    // 7 Units
    // Unit Caps [20 15 35 40 15 15 10]
    // Max Capacity of Plant - 150 MW
    // Net_Reserve = MaxCapacity - Capacity_down - Load
    // Each unit has 4 slots
    // Loads [80 90 65 70]
    // Nd = 28
    // Unit commitment - Needs start up/shut down costs
    double generatorScheduling(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j) {
        std::vector<double> unitCaps(7, 0);
        std::vector<double> loads(4, 0);
        double maxCap, totalCap, Z;
        bool valid;

        Z = 0;
        maxCap = 150; totalCap = 0;
        unitCaps[0] = 20; unitCaps[1] = 15; unitCaps[2] = 35;
        unitCaps[3] = 40; unitCaps[4] = 15; unitCaps[5] = 15;
        unitCaps[6] = 10; 

        loads[0] = 80; loads[1] = 90; loads[2] = 65; loads[3] = 70;

        // Capacity
        for(int k=0; k<loads.size(); k++){
            totalCap  = Probes[j][p]->Position[k+0]  * unitCaps[0];
            totalCap += Probes[j][p]->Position[k+4]  * unitCaps[1];
            totalCap += Probes[j][p]->Position[k+8]  * unitCaps[2];
            totalCap += Probes[j][p]->Position[k+12] * unitCaps[3];
            totalCap += Probes[j][p]->Position[k+16] * unitCaps[4];
            totalCap += Probes[j][p]->Position[k+20] * unitCaps[5];
            totalCap += Probes[j][p]->Position[k+24] * unitCaps[6];

            if(totalCap  >= loads[k]){
                Z += maxCap - (maxCap-totalCap) - loads[k];
            }
        }

        // Valid Maintenance
        valid = true;
        for(int i=0; i<2; i++){ // For each Unit
            for(int k=0; k<3; k++){
                if(Probes[j][p]->Position[k+(4*i)] == 0  && Probes[j][p]->Position[(k+1)+(4*i)] == 1){
                    valid = false;
                }
            }
        }
        if(!valid){Z = 0;}
        return Z;
    }

    // Alternating
    // Max Fitness = Nd
	double binaryF10(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j) {
	    double Z = 0;

	    for(int i=0; i<Nd; i++){
		    if(Probes[j][p]->Position[i] == 1 && i%2 == 0) { Z = Z+1;}
            if(Probes[j][p]->Position[i] == 0 && i%2 == 1) { Z = Z+1;}
	    }

	    return Z;
    }

    // 
    // Sphere, unimodal
    // Evaluated on: [-5.12, 5.12]
    //
	double F1(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j) { // Sphere, Unimodal
		double Z=0,  Xi;

		for(int i=0; i<Nd; i++){
			Xi = Probes[j][p]->Position[i];
			Z = Z + pow(Xi,2);
		}
		return -Z;
	}
	
	// 
	// Rastrigin, multi modal
	// Evaluated on: [-5.12, 5.12]
	// Global Minimum: f(x*) = 0, at x* = (0,...,0)
	//
	double F2(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j) { // Rastrigin, MultiModal
		double Z=0,  Xi;
		
		for(int i=0; i<Nd; i++){
			Xi = Probes[j][p]->Position[i];
			Z += (pow(Xi,2) - 10 * cos(2*Pi*Xi) + 10);
		}
		return -Z;
	}
	
	//
	// Griewank, multi modal
	// Evaluated on: [-600, 600]
	// Global Minimum: f(x*) = 0, at x* = (0,...,0)
	//
	double F3(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j) { // Griewank, MultiModal
		double Z, Sum, Prod, Xi;

		Z = 0; Sum = 0; Prod = 1;
    
		for(int i=0; i<Nd; i++){
			Xi = Probes[j][p]->Position[i];
			Sum  += Xi*Xi;
			Prod *= cos(Xi/sqrt((double)i)); 
		
            #ifdef _WIN32
	            if(_isnan(Prod)) Prod = 1;
            #else
                if(std::isnan(Prod)) Prod = 1;
            #endif
		}
	
		Z = 1 + Sum/4000.0f - Prod;
	
		return -Z;
	}
	
	//
	// Rosenbrock, uni modal
	// Evaluated on: [-5,10]
	// Global Minimum: f(x*) = 0, at x* = (1,...,1)
	//
	double F4(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j) { // Rosenbrock, UniModal
		double Z=0, Xi, XiPlus1;

		for(int i=0; i<Nd-1; i++){
			Xi = Probes[j][p]->Position[i];
			XiPlus1 = Probes[j][p]->Position[i+1];
			Z = Z + pow(100 * pow(XiPlus1-pow(Xi,2),2)+(Xi-1),2);
		}
		return -Z;
	}

    //
    // Ackley, multi modal
    // Evaluated on: x_i in [-32.768, 32.768], for all i in 1,...,d
    // Global Minimum: f(x*) = 0, at x* = (0,...,0)
    //
	double F5(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){ // Ackley, Multimodal
		// -32 <= Xi <= 32
		double Z = 0;
		double term1 = 0, term2 = 0;
		for(int i = 0; i < Nd; i++){
			double Xi = Probes[j][p]->Position[i];
			term1 = term1 + pow(Xi, 2);
			term2 = term2 + cos(TwoPi * Xi);
		}
		term1 = -0.2 * sqrt( (1/Nd) * term1);
		term2 = (1/Nd) * term2;
		Z = -20 * pow(e, term1) - pow(e, term2) + 20 + e;
		
		return -Z;
	}
    //
    // Exponential, Unimodal
    //
	double F6(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){ // Exponential, UniModal
		// -5.12 <= Xi <= 5.12
		double Z = 0;
		double sum = 0, sum_alpha = 0;
		for(int i = 0; i < Nd; i++){
			double Xi = Probes[j][p]->Position[i];
			sum = sum + pow(e, i * Xi) * i;
			sum_alpha = sum_alpha + pow(e, -5.12 * i);
		}
		Z = sum - sum_alpha;
		return -Z;
	}
	
	//
	// Lunacek's bi-Rastrigin, multi modal
	// Evaluated on: [-5.12, 5.12]
	// Global Minimum: f(x*) = 0, at x* = u1 
	//
	double F7(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){ // Lunacek, MultiModal
		double Z = 0,		u1 = 2.5,		d	= 1;
		double func1 = 0,		func2 = 0,	term2 = 0;
		double s  = 1 - (1/ (2*sqrt(Nd + 20) - 8.2));
		double u2 = -1*sqrt( ( pow(u1, 2) - d) / s );
			
		for(int i = 0; i < Nd; i++){
			double Xi = Probes[j][p]->Position[i];
			func1 = func1 + pow(Xi - u1, 2);
			func2 = func2 + pow(Xi - u2, 2);
			term2 = term2 + 1 - cos(TwoPi*(Xi - u1));
		}
		double term1 = std::min(func1, d*Nd + s*func2);
		Z = term1 + 10*term2;
		return -Z;
	}
	//
	// Schwefel 1.2 ("Double Sum" or "Ridge") 
	// Evaluated on: [-100, 100]
	// Global Minimum: f(x*) = 0, at x* = 0
	//
	double F8(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){ // Ridge
		// -64 <= Xi <= 64		
		double Z = 0;

		for(int i = 0; i < Nd; i++){
			double sum_inner = 0;

			for(int k = 0; k < i; k++){
				double Xi = Probes[j][p]->Position[k];
				sum_inner = sum_inner + Xi;
			}
			Z = Z + pow(sum_inner, 2);
		}
		return -Z;
	}
	
	//
	// Schaffer's F7, Multi Modal
	// Evaluated on: [-100, 100]
	// Global Minimum: f(x*) = 0, at x* = 0
	//  
	double F9(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){ // Schaffer's F7, MultiModal
			// -100 <= Xi <= 100
			double Z = 0;
			for(int i = 0; i < Nd-1; i++){
				double Xi = Probes[j][p]->Position[i];
				double Xi_plus1 = Probes[j][p]->Position[i+1];
				double Si = sqrt( pow(Xi, 2) + pow(Xi_plus1, 2));

				Z = Z + pow( (1/(Nd-1)) * sqrt(Si) * (sin(50.0*pow(Si, 0.2) + 1)), 2);
			}
			return -Z;
	}

	//
	// Beale, Multi Modal
	// Dimensions: 2
	// Evaluated on: [-4.5, 4.5]
	// Global Minimum: f(x*)=0 at x* = (3, 0.5)
	//
	double F10(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){  // Beale 

		double x = Probes[j][p]->Position[0]; 
 		double y = Probes[j][p]->Position[1];
 		double Z = 0;

		double term1 = pow(1.5   - x + x * y, 2);
		double term2 = pow(2.25  - x + x * pow(y, 2), 2);
		double term3 = pow(2.625 - x + x * pow(y, 3), 2);
		Z = (term1 + term2 + term3);
		
		return -Z;
	}

	
	//
	// Booth, Multi modal
	// Dimensions: 2
	// Evaluated on: [-10, 10]
	// Global Minimum : f(x) = 0 at x* = (1,3)
	//
	double F11(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){  // Booth

		return -1.0L * pow(Probes[j][p]->Position[0] + 2 * Probes[j][p]->Position[1] - 7, 2) + pow((2 * Probes[j][p]->Position[0] + Probes[j][p]->Position[1] - 5), 2);

	}
	
	//
	// Bukin, MultiModal
	// Dimensions: 2
	// Evaluated on x_1 = [-15, -5], x_2 = [-3,-3]
	// Global Minimum: f(x*) = 0, at x* = (-10,1)
	//
	double F12(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){  // Bulkin
		return -(100 * sqrt( std::abs( Probes[j][p]->Position[1] - 0.01 * pow( Probes[j][p]->Position[0], 2 ) ) ) + 0.01 * std::abs(Probes[j][p]->Position[0] + 10) );
	}

	//
	// 6 Hump Camel
	// Dimensions: 2
	// Evaluated on: x1 = [-3, 3] , x2 = [-2, 2] 
	// Global Minimum: f(x*) = -1.316, at x* =(0.0898, -0.7126) and (-0.0898, 0.7126)
	//
	double F13(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){  // 6 Hump Camel
		double term1 = (4 - 2.1 * pow(Probes[j][p]->Position[0], 2) + pow(Probes[j][p]->Position[0], 4) / 3 ) * pow(Probes[j][p]->Position[0], 2);
		double term2 = Probes[j][p]->Position[0] * Probes[j][p]->Position[1];
		double term3 = (-4 + 4 * pow(Probes[j][p]->Position[1], 2)) * pow(Probes[j][p]->Position[1], 2);
		return -(term1 + term2 + term3);
	}
	//
	// Easom 
	// Dimensions: 2
	// Evaluated on: [-100,100]
	// Global minimum: f(x*) = -1, at x* =(pi, pi)
	//
	double F14(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){  // Easom
		double exponent = pow(Probes[j][p]->Position[0] - Pi, 2) + pow(Probes[j][p]->Position[1] - Pi, 2);
		return cos(Probes[j][p]->Position[0])*cos(Probes[j][p]->Position[1])* pow( e, -exponent );
	}
//
// Goldstein-Price
// Dimensions: 2
// Evaluated on: [-2, 2]
// Global Minimum: f(x*) = 3, at x* = (0,-1) 
//
	double F15(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){  // Goldstein-Price
		double x = Probes[j][p]->Position[0];
		double y = Probes[j][p]->Position[1];

		double term1_1 = pow((x + y + 1),2);
		double term1_2 = 19 - 14 * x + 3 * pow(x, 2) - 14 * y + 6 * x * y + 3 * pow(y, 2);
		double term2_1 = pow((2 * x - 3 * y), 2);
		double term2_2 = 18 - 32 * x + 12 * pow(x, 2) + 48 * y - 36 * x * y + 27 * pow(y,2);
		return -(1 + term1_1 * term1_2)*(30 + term2_1 * term2_2);
	}

//
// Levy N. 13,  Multi Modal 
// Dimensions: 2	
// Evaluated on: [-10, 10]
// Global Minimum: f(x*) = 0 at x* = (1,1)
//
	double F16(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){  // Levi
		double term1 = pow( sin(3 * Pi * Probes[j][p]->Position[0]), 2);
		double term2 = pow( (Probes[j][p]->Position[0] - 1.0), 2);
		double term3 = (1 + pow(sin(3 * Pi * Probes[j][p]->Position[1]), 2));
		double term4 = pow((Probes[j][p]->Position[1] - 1.0), 2);
		double term5 = (1 + pow(sin(2 * Pi * Probes[j][p]->Position[1]),2));
		return -(term1 + term2 * term3 + term4 * term5 );
	}
//
// Matya 
// Dimensions: 2 
// Evaluated on: [-10,10]
// Global Minimum: f(x*)=0, at x* = (0,0)
//
	double F17(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){  // Matya
		return -( 0.26 * ( pow(Probes[j][p]->Position[0], 2) + pow(Probes[j][p]->Position[1], 2) ) - (0.48 * Probes[j][p]->Position[0] * Probes[j][p]->Position[1] ) );
	}

	//
	// Modified Double sum
	// Evaluated on : [-10, 10]
	// Minimum: f(1,2,3,...,n) = 0
	//
	double F18(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){  // Modified Double Sum
		double y = 0;
		for( int i = 0 ; i < Nd; i ++){
			for( int k = 0; k < i + 1; k++){
				y += pow(( Probes[j][p]->Position[k] - ( k + 1 ) ), 2);
			}
		}
		return -y;
	}

	//
	// Quartic
	// Evaluated on: [-1.28, 1.28]
	// Minimum: f(0,...,0) = 0 + noise
	// 
	double F19(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){  // Quartic
		MTRand mt;
		double y = 0.0;
		for(int i = 0; i < Nd; i++ ){
			y+= mt.rand() + (i + 1.0) * pow(Probes[j][p]->Position[i], 4.0 );
		}
		return -(y + mt.rand()) ;
	}

	
	//
	// Rana, multi modal
	// Dimensions: 2
	// Evaluated on: [-512, 512]
	// Minimum: f(-488.6326, 512) = -511.73
	//
	double F20(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){  // Rana
		double sub_x0 = sqrt( std::abs( Probes[j][p]->Position[1] + 1 - Probes[j][p]->Position[0] ) );
		double add_x0 = sqrt( std::abs( Probes[j][p]->Position[1] + 1 + Probes[j][p]->Position[0] ) );
		double term1 = Probes[j][p]->Position[0] * sin( sub_x0 ) * cos ( add_x0 );
		double term2 = (Probes[j][p]->Position[1] + 1) * cos( sub_x0 ) * sin ( add_x0 );
		return - ( term1 + term2 );
	}
	//
	// Schwefel, multi modal
	// Evaluated on: [-512, 512]
	// Minimum: f(420.968746, 420.968746,..., 420.968746) = 0
	//
	double F21(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){  // Schwefel
		double a = 419.982887;
		double y = 0; 
		for ( int i = 0; i < Nd; i++){
			y+= Probes[j][p]->Position[i] * ( sin ( sqrt( std::abs(Probes[j][p]->Position[i]))));
		}
		return -(a * Nd - y);
	}

	//
	// Schwefel 2.22
	// Evaluated on: [-10, 10]
	// Minimum: f(0,...,0) = 0
	//
	double F22(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){  // Schwefel 222
		double add = 0;
		double mult = 0;
		for ( int i = 0 ; i < Nd ; i++ ){
			add  += std::abs( Probes[j][p]->Position[i] );
			mult *= std::abs( Probes[j][p]->Position[i] );
		}
		return -(add + mult);
	}

	//
	// Whitley, multi modal
	// Evaluated on: [-10.24, 10.24]
	// Minimum: f(1,...,1) = 0
	//
	double F23(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j){  // Whitley
		double y = 0;

		for (int i = 0 ; i < Nd; i++ ){
			for( int k = 0; k < Nd; k++){
				double temp1 = 100 * pow( pow( Probes[j][p]->Position[i], 2 ) - Probes[j][p]->Position[k], 2 );
				double temp2 = pow(1 - Probes[j][p]->Position[k], 2);
				double term1 = pow( temp1 + temp2, 2) / 4000.0f;
				double term2 = cos( temp1 + temp2 );
				y += term1 - term2 + 1 ;
			}
		}
		
		return -y;
	}

	double irs(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j) {  return 0;		}
}
