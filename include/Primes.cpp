/* This file is part of CFO in C++.

    CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
	
	Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#include "Primes.h"

Primes::Primes(){
	currentPrime = 0;
}
		
Primes::Primes(int startingNum){
	currentPrime = startingNum;
}

bool Primes::isPrime(int x){
    
	if (x < 2){ return false;}
    
	for(int i=2; i<x; i++){
        if (x % i == 0){ return false; }
	}
    return true;
}

double Primes::nextPrime(){
	currentPrime++;
	for (; !isPrime(currentPrime); ++currentPrime){}
    
	return currentPrime;
}

double Primes::getNthPrime(int N){
	int numPrimes = 0;
	int curPrime;

	for(int i=2; ; i++){

		if(isPrime(i)){
			numPrimes++;
			curPrime = i;
		}

		if(numPrimes >= N){break;}
	}
	return curPrime;
}