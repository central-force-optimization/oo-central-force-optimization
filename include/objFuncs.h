#ifndef OBJFUNCS_H_
#define OBJFUNCS_H_

#include <vector>
#include <iostream>
#include <memory>
#include "MTRand.h"
#include "Probe.h"

namespace ObjFuncs{

    static const double Pi	  = 3.141592653589793238462643;
    static const double TwoPi = 2 * Pi;
    static const double e	  = 2.718281828459045235360287;

    extern double irs(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);

    extern double Alt(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double generatorScheduling(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);

    extern double binaryF1(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double binaryF2(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double binaryF3(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double binaryF4(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double binaryF5(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);

    extern double decode(int start, int stop, std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int p, int j);
    extern double binaryF6(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j); //Sphere function
    extern double binaryF7(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j); /* 2 variables, 12 bits/variable. */
    extern double binaryF8(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j); /* 5 variables, 10 bits/variable. */
    extern double binaryF9(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j); /* 30 variables, 8 bits/variable. ssrand() needs to be defined to be a (0,1) gaussian random variable */
    extern double binaryF10(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double binaryF11(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);

    extern double F1(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F2(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F3(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F4(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
		
    extern double F5(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F6(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F7(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F8(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F9(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);

    extern double F10(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F11(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F12(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F13(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F14(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F15(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F16(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F17(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F18(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F19(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F20(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F21(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F22(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    extern double F23(std::vector <std::vector<std::shared_ptr<Probe>>>& Probes, int Nd, int p, int j);
    

}
#endif