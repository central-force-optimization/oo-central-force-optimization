#ifndef FITNESS_FUNCTION_H_
#define FITNESS_FUNCTION_H_

#include "defs.h"
#include <string>
#include "Probe.h"
#include <memory>

class fitnessFunction{

    public:
        fitnessFunction(){

        };

        fitnessFunction(double (*rPtr)(std::vector <std::vector<std::shared_ptr<Probe>>>&, int, int, int), int min, int max, std::string name){
            _rPtr = rPtr;
            _min  = min;
            _max  = max;
            _name = name;
        };

        
        double (*fFunction())(std::vector <std::vector<std::shared_ptr<Probe>>>&, int, int, int)
        { 
            return _rPtr; 

        };
		double min() { return _min; };
        double max() { return _max;};
        std::string name() { return _name;};

    private:
        double (*_rPtr)(std::vector <std::vector<std::shared_ptr<Probe>>>&, int, int, int);
        double _min, _max;
        std::string _name;
};

#endif