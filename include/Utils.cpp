/* This file is part of CFO in C++.

    CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#include "Utils.h"
#include <iostream>

namespace Utils{
    int callback(void *NotUsed, int argc, char **argv, char **azColName){
        int i;
        for(i=0; i<argc; i++){
            printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        }
        printf("\n");
        return 0;
    }

    void setupDb(){
        sqlite3 *db;
        char *zErrMsg = 0;
        int rc;
        std::string sql;

        /* Open database */
        rc = sqlite3_open("CFO.db", &db);
        if(rc){
            fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
            exit(0);
        }
        // else{
        //     fprintf(stderr, "Opened database successfully\n");
        // }

       /* Create SQL statement */
       sql = "CREATE TABLE IF NOT EXISTS Results("  \
                "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," \
                "Function           TEXT    NOT NULL," \
                "Fitness            REAL    NOT NULL," \
                "BestProbe          INT     NOT NULL,"\
                "bestTimeStep       INT     NOT NULL,"\
                "Gamma              REAL    NOT NULL,"\
                "NpNd               INT     NOT NULL,"\
                "Np                 INT     NOT NULL,"\
                "Nd                 INT     NOT NULL,"\
                "LastStep           INT     NOT NULL,"\
                "Evals              INT     NOT NULL,"\
                "Corrections        INT     NOT NULL,"\
                "posTime            REAL    NOT NULL,"\
                "corTime            REAL    NOT NULL,"\
                "fitTime            REAL    NOT NULL,"\
                "accelTime          REAL    NOT NULL,"\
                "shrinkTime         REAL    NOT NULL,"\
                "convTime           REAL    NOT NULL,"\
                "totalTime          REAL    NOT NULL,"\
                "threads            INT     NOT NULL,"\
                // 2016-04-28 Logging for CFO and IPD Type
                "cfoType            TEXT    NOT NULL,"\
                "ipdType            TEXT    NOT NULL,"
                "insertedOn         TIMESTAMP DEFAULT CURRENT_TIMESTAMP);";

        rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
        
        if(rc != SQLITE_OK){
            fprintf(stderr, "SQL error: %s\n", zErrMsg);
            sqlite3_free(zErrMsg);
        }
        
        sqlite3_close(db);
    }

    void recordValues(std::string functionName, double fitness, int bestProbe, int bestTimeStep, 
        double Gamma, int NpNd, int Np, int Nd, int lastStep, int Evals, int Corrections, 
        double posTime, double cortime, double fitTime, double accelTime, double shrinkTime, 
        double convTime, double totalTime, int threads, CFO_TYPE cfoType, IPD_TYPE ipdType){

        sqlite3 *db;
        char *zErrMsg = 0;
        int rc;
        std::string sql;

       /* Open database */
        rc = sqlite3_open("CFO.db", &db);
        if(rc){
            Utils::setupDb();
            fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
            exit(0);
        }

       /* Create SQL statement */
        // sql = ;

        std::ostringstream ss;

        ss <<"INSERT INTO Results (id, Function, Fitness, bestProbe, bestTimeStep, Gamma, NpNd, Np, Nd, LastStep, Evals, Corrections, "\
                                  "posTime, corTime, fitTime, accelTime, shrinkTime, convTime, totalTime, threads, cfoType, ipdType) ";    
        ss << "VALUES (NULL, '";
        ss << functionName                      << "', ";
        ss << (long double)     fitness         << ", ";
        ss << (long long int)   bestProbe       << ", ";
        ss << (long long int)   bestTimeStep    << ", ";
        ss << (long double)     Gamma           << ", ";
        ss << (long long int)   NpNd            << ", ";
        ss << (long long int)   Np              << ", ";
        ss << (long long int)   Nd              << ", ";
        ss << (long long int)   lastStep        << ", ";
        ss << (long long int)   Evals           << ", ";
        ss << (long long int)   Corrections     << ", ";
        ss << (long double)     posTime         << ", ";
        ss << (long double)     cortime         << ", ";
        ss << (long double)     fitTime         << ", ";
        ss << (long double)     accelTime       << ", ";
        ss << (long double)     shrinkTime      << ", ";
        ss << (long double)     convTime        << ", ";
        ss << (long double)     totalTime       << ", ";
        ss << (int)             threads         << ", ";

        // 2016-04-28 Logging for CFO and IPD Type
        ss << "'" << Utils::cfoTypeString(cfoType)   << "', ";
        ss << "'" << Utils::ipdString(ipdType)       << "'";
        ss << ");";
        sql = ss.str();

       /* Execute SQL statement */
       rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
       if( rc != SQLITE_OK ){
          fprintf(stderr, "SQL error: %s\n", zErrMsg);
          sqlite3_free(zErrMsg);
       }else{
          // fprintf(stdout, "Records created successfully\n");
       }
       sqlite3_close(db);
    }

    std::vector<double> decToBin(double n, int numDigits) {
        std::vector<double> retVal;


        if(n <= 1) {
            retVal.push_back(n);
        }else{
            while(n > 0){
                retVal.push_back(floor(fmod(n, 2)));
                //retVal.push_back((int)n % 2);
                n = floor(n/2);  
            }
        }

        while(retVal.size() < (unsigned)numDigits){
            retVal.push_back(0);
        }
        reverse(retVal.begin(), retVal.end());
        return retVal;
    }
    std::vector < std::vector < double > > latinHyperCube_Random(int Nv, int Ns, MTRand& mt){
        
        std::vector < std::vector < double > > lhsMatrix (Ns, std::vector<double>(Nv, 0));
        double segmentSize = 1.0/(float)Ns;
        double curSegment = 0.0;
        int a;
        double c;

        // Create Initial Matrix
        for(int i=0; i<Ns; i++){
            for(int j=0;j<Nv; j++){
                lhsMatrix[i][j] = ((i+1)-1+mt.rand())/Ns;
            }
            curSegment += segmentSize;
        }

        // Randomly match
        for(int i=0;i<Nv; i++){
            for(int j=0; j<Ns; j++){
                a = mt.randInt(Ns-1); 
                c = lhsMatrix[j][i]; 
                lhsMatrix[j][i] = lhsMatrix[a][i]; 
                lhsMatrix[a][i] = c;
            }
        }

        return lhsMatrix;
    }
    std::vector < std::vector < double > > descriptiveSampling_Random(int Nv, int Ns, MTRand& mt){
        
        std::vector < std::vector < double > > dsMatrix (Ns, std::vector<double>(Nv, 0));
        int a;
        double c;

        // Create Initial Matrix
        for(int i=0; i<Ns; i++){
            for(int j=0;j<Nv; j++){
                dsMatrix[i][j] = (i+1-0.5)/Ns;
            }
        }

        // Randomly match
        for(int i=0;i<Nv; i++){
            for(int j=0; j<Ns; j++){
                a = mt.randInt(Ns-1); 
                c = dsMatrix[j][i]; 
                dsMatrix[j][i] = dsMatrix[a][i]; 
                dsMatrix[a][i] = c;
            }
        }

        return dsMatrix;
    }
    
    std::vector < std::vector < double > > haltonSampling(int Nd, int Ns){
        std::vector < std::vector < double > > haltonMatrix(Ns, std::vector<double>(Nd, 0));
        Primes p;
        double curPrime;

        for(int j=0; j<Nd; j++){
            curPrime = p.nextPrime();
            for(int i=0; i<Ns; i++){
                haltonMatrix[i][j] = Utils::corputBase(curPrime, i+1);
            } 
        }
        return haltonMatrix;
    }
    std::vector < std::vector < double > > hammersleySampling(int Nd, int Ns){
        std::vector < std::vector < double > > hMatrix(Ns, std::vector<double>(Nd, 0.0));
        Primes p;
        double curPrime;

        for(int j=1; j<Nd; j++){ // Column
            curPrime = p.nextPrime();
            for(int i=1; i<Ns; i++){ // Row
                hMatrix[i][j] = Utils::corputBase(curPrime, i-1);
            }
        }
        for(int i=0; i<Ns; i++){ // Row
            hMatrix[i][0] = i/(double)Ns;
        }
        return hMatrix;
    }
    std::vector < std::vector < double > > faureSampling(int Nd, int Ns){
        std::vector < std::vector < double > >	bb(Ns, std::vector<double>(Nd ,0.0));
        std::vector < std::vector < double > >	fMatrix(Ns, std::vector<double>(Nd,0.0));
        std::vector < std::vector < int > >		a(Ns, std::vector<int>(Nd,0));
        std::vector < std::vector < int > >	G;	
        Primes prime(Nd-1);
        double curBase;
        int r;

        curBase = prime.nextPrime();
        r		= (int)floor(log((double)Ns)/log(curBase))+1;
        G.resize(r, std::vector<int>(r, 0));

        for(int j=0; j<r; j++){
            for(int i=0; i<j+1; i++){
                G[j][i] = ((int)combination(j, i)) % ((int)curBase);
            }
        }

        for(int i=0; i<Ns; i++){
            for(int j=0; j<Nd; j++){
                bb[i][j] = 1.0/pow(curBase, j+1);
            }
        }

        for(int i=0; i<Ns; i++){
            for(int j=0; j<Nd; j++){
                a[i][j] = i;
            }
        }

        for(unsigned int i=0; i<a.size(); i++){
            a[i] = Utils::changeBase(i, curBase, r);
        }

        for(int i=0; i<Ns; i++){
            fMatrix[i][0] = corputBase(curBase, i);
        }

        for(int i=1; i<Nd; i++){ 		
            a = matMult(a, G);

            for(unsigned int row=0; row<a.size(); row++){
                for(unsigned int col=0; col<a[row].size(); col++){
                    a[row][col] = a[row][col] % (int)curBase;
                }
            }

            for(unsigned int m=0; m<fMatrix.size(); m++){
                for(int n=0; n<r; n++){
                    fMatrix[m][i] += a[m][n] * bb[m][n];
                }
            }
        }

        bb.clear(); a.clear(); G.clear();	
        return fMatrix;
    }		
    double corputBase(double base, double number){
        double h, ib;
        double i, n0, n1;

        n0	= number; 
        h	= 0;
        ib	= 1.0/base;

        while(n0 > 0){
            // Equation 1 - n = sum_{j=0}^{m}{a_{j}(n) * b^{j}}
            n1	= (int)(n0/base);
            i	= n0 - n1 * base;

            // Equation 2 - b(n) = sum_{j=0}^{m}{a_{j}(n) * b^{-j-1}}
            h	= h + ib * i;
            ib	= ib / base;
            n0	= n1;
        }
        return h;
    }

    std::vector< std::vector<int> > matMult(std::vector< std::vector<int> > A, std::vector< std::vector<int> > B){
        std::vector< std::vector<int> > C(A.size(), std::vector<int>(B.size(), 0));

        int sum = 0;

        for(unsigned int i=0; i<A.size(); i++){
            for(unsigned int j=0; j<B[0].size(); j++){
                sum = 0;       
                for(unsigned int k=0; k<A[0].size(); k++){
                    sum = sum + A[i][k]*B[k][j];
                }
                C[i][j] = sum;
            }
        }
        return C;
    }
    std::string toLower(std::string str) {
        for (unsigned int i=0;i<str.length();i++){
            str[i] = tolower(str[i]);
        }
        return str;
    }
    std::string toUpper(std::string str) {
        for (unsigned int i=0;i<str.length();i++){
            str[i] = toupper(str[i]);
        }
        return str;
    }
    std::vector<std::string> permuteCharacters(std::string topermute){

        std::vector<char> iV;
        std::vector<std::string> results;
        std::string s;

        for(unsigned int x=0; x< topermute.length(); x++){
            iV.push_back(topermute[x]);
        }

        std::sort(iV.begin(), iV.end());
        s.assign(iV.begin(), iV.end());
        results.push_back(s);

        while (std::next_permutation(iV.begin(), iV.end())){
            s.assign(iV.begin(), iV.end());
            results.push_back(s);
        }
        return results;
    }
    double sigMoid(double v){
        return 1/(1+exp(-v));
    }
    void tokenizeString(std::string str,std::vector<std::string>& tokens, const std::string& delimiter = " "){

        int pos = 0;
        std::string token;

        for(;;){

            pos = str.find(delimiter);

            if(pos == (int)std::string::npos){
                tokens.push_back(str);
                break;
            }

            token = str.substr(0, pos);
            tokens.push_back(token);
            str = str.substr(pos+1);
        }
    }
    std::vector<int> changeBase(double num, double base, int numDigits){
        std::vector<int> retValue;
    
        do{
            retValue.push_back((int)num % (int)base);
            num = (int)(num/base); 
        }while(num != 0); 
 
        while((int)retValue.size() < numDigits){
            retValue.push_back(0);
        }

        return retValue;
    }
    std::string changeBase(std::string Base, int number){
        std::stringstream ss;

        ss << Base << number;
        return ss.str();
    }
    int factorial(int n){
        int result=1;

        for (int i=1; i<=n; ++i){
            result=result*i;
        }

        return result;
    }
    int combination(int n, int r){
        return Utils::factorial(n)/(Utils::factorial(r) * Utils::factorial(n-r));
    }
    std::string vectorToString(std::vector<double> v){
        std::string result;
        char buf[1];

        result = "";
        for(unsigned int x=0; x< v.size(); x++){
            sprintf(buf, "%i", (int)v[x]);
            result += buf[0];
        }
        return result;
    }
    std::string vectorToString(std::vector<int> v){
        std::string result;
        char buf[1];

        result = "";
        for(unsigned int x=0; x< v.size(); x++){
            sprintf(buf, "%i", v[x]);
            result += buf[0];
        }
        return result;
    }
    std::string arrayToString(int* v, int length){
        std::string result;
        char buf[1];

        result = "";
        for(int x=0; x< length; x++){
            sprintf(buf, "%i", v[x]);
            result += buf[0];
        }
        return result;
    }
    char* getTimeStamp(){
        time_t now;
        char* aTime = new char[20];

        time(&now);
        struct tm * timeinfo = localtime(&now);;
        strftime(aTime, 20, "%m_%d_%Y_%H_%M_%S", timeinfo);

        return aTime;
    }
    void twoByTwoCholeskyDecomp(std::vector<std::vector<double> >& A){

        std::vector < std::vector < double > > L(A.size(),std::vector < double > (A.size(),0));
        L[0][0] = sqrt(A[0][0]);
        L[0][1] = 0;
        L[1][0] = A[0][1]/L[0][0];
        L[1][1] = sqrt(A[1][1]-(L[1][0]*L[1][0]));
        A = L;
    }

    std::string ipdString(IPD_TYPE ipdType){
        std::string retValue;

        switch(ipdType){
            case UNIFORM_ON_DIAGONAL:	retValue = "UOD";		break;
            case UNIFORM_ON_AXIS:		retValue = "UOA";		break;
            case LATIN_HYPERCUBE:		retValue = "LHS";		break;
            case DESC_SAMP:				retValue = "DS";		break;
            case LDS_HALTON:			retValue = "LDS_HAL";	break;
            case LDS_HAMMERSLEY:		retValue = "LDS_HAM";	break;
            case LDS_FAURE:				retValue = "LDS_FAU";	break;
            case LDS_HALTON_TRANS:		retValue = "LDA_HAL_T";	break;
            case LDS_HAMMERSLEY_TRANS:	retValue = "LDA_HAM_T";	break;
            case LDS_FAURE_TRANS:		retValue = "LDA_FAU_T";	break;
            case RANDOM:				retValue = "RANDOM";	break;
            case RANDOM_VDC:			retValue = "RANDOM_VDC";break;
            default:					retValue = "";
        }
        return retValue;
    }

    std::string cfoTypeString(CFO_TYPE ct){
        std::string retValue;

        switch(ct){
            case STANDARD:		    retValue = "STANDARD";		    break;
            case RING:			    retValue = "RING";			    break;
            case LINEAR:		    retValue = "LINEAR";		    break;
            case STAR:			    retValue = "STAR";			    break;
            case MESH:			    retValue = "MESH";			    break;
            case STATIC_TREE:	    retValue = "STATIC_TREE";	    break;
            case TOROIDAL:		    retValue = "TOROIDAL";		    break;
            case BINARY:		    retValue = "BINARY";		    break;
            case CUDA:              retValue = "CUDA";              break;
            case GLOBAL_BEST:       retValue = "GLOBAL_BEST";       break;
            case GLOBAL_BESTWORST:  retValue = "GLOBAL_BEST_WORST"; break;
            case MOCFO:             retValue = "MO_CFO";            break;
			case MULTIPLICITY:      retValue = "MULTIPLICITY";      break;
            default:			    retValue = "";				    break;
        }
        return retValue;
    }   
   
    void setOptions(AnyOption* opt){

        opt->addUsage( "Usage: " );
        opt->addUsage( "" );
        opt->addUsage( "--help          Prints this help");
        opt->addUsage( "" );
       
        opt->addUsage("Parameters with Values: " );
        opt->addUsage(" --threads        # of threads to use with OpenMP ");
        opt->addUsage(" --Np             # of probes ");
        opt->addUsage(" --Nd             # of dimensions ");
        opt->addUsage(" --Nt             # of test  ");
        opt->addUsage(" --alpha          Value for alpha ");
        opt->addUsage(" --beta           Value for beta ");
        opt->addUsage(" --iters          # of iterations/trials ");

        opt->addUsage("Boolean Flags: " );
        opt->addUsage(" --optimalNp      Sets Np to the optimal value per test function given Nd = 10, 20, 30.");
        opt->addUsage(" --threadPerProbe    Fixes threads based on Np value ");
        opt->addUsage(" --runPF             Runs PF_CFO prior to running CFO");
        opt->addUsage(" --basicTest         Runs benchmarking for Basic CFO Algorithm");
        opt->addUsage(" --openMpTest        Runs benchmarking for OpenMP CFO Algorithm");
        opt->addUsage(" --neighborhoodTest  Runs benchmarking for Neighborhood variants of CFO Algorithm");
        opt->addUsage(" --binaryTest        Runs benchmarking for Binary CFO Algorithm");
        opt->addUsage(" --threadTest        Runs thread test for CFO Algorithm");
		opt->addUsage(" --multTest          Runs Multiplicity test for CFO Algorithm");

        opt->addUsage( "" );

        
        opt->setOption("Np");       opt->setOption("Nd");   opt->setOption("Nt");
        opt->setOption("alpha");    opt->setOption("beta");
        opt->setOption("threads");  opt->setOption("iters");

        opt->setFlag("help");
        opt->setFlag("nested");
        opt->setFlag("threadPerProbe");
        opt->setFlag("optimalNp");
        opt->setFlag("runPF");

        opt->setFlag("basicTest");
        opt->setFlag("openMpTest");
        opt->setFlag("neighborhoodTest");
        opt->setFlag("binaryTest");
        opt->setFlag("threadTest");
		opt->setFlag("multTest");
    }

}
