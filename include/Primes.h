/* This file is part of CFO in C++.

    CFO in C++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CFO in C++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with CFO in C++.  If not, see <http://www.gnu.org/licenses/>.
	
	Copyright 2009 - 2012 Robert C. Green II - Robert.C.Green@gmail.com
*/

#ifndef PRIMES_H_
#define PRIMES_H_

class Primes{
	
	public : 	
		Primes();
		Primes(int startingPrime);
		double nextPrime();
		double getNthPrime(int N);

	private:
		bool isPrime(int x);
		int  currentPrime;
};

#endif