import sqlite3
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
from datetime import datetime
from matplotlib.collections import PolyCollection
from matplotlib.colors import colorConverter
from mpl_toolkits.mplot3d import Axes3D
import mpl_toolkits.mplot3d.art3d as art3d
import time

#fig = plt.figure()
#ax  = fig.gca(projection='3d')


'''
    Reads the database @directory, and plots the data, given @thr_max, @thr_min, 
    and the distance between threads, @thr_stride
'''


def read_and_create_graphs ( directory, thr_max=12, thr_min=1, thr_stride = 1 ) : 
    r      = []                         # holds the results. 
    #Nds  = ['20', '30']              # values for Nd
    Nds    = ['30']
    fcns_i = range(1,24)
    fcns_i.pop(18)
    fcns_i = [1,2,3,4,7,13,15,18, 22]
    #fcns_i=[5,6,8,9,10,11,12,14,16,17, 20, 21, 23]
    fcns = ['F'+`x` for x in fcns_i] # all the function names
    #fcns.pop(18)                           # remove f19. it's random and scary
    con  = sqlite3.connect(directory)
    #fcns   = ['F20']
    print(fcns)
    cur  = con.cursor()
    i    = 0                               # Nd  index
    j    = 0                               # fcn index
    # Build up r. 
    # r[Nds][fcns][nested?][threads]
    serial  = []
    speedup = []
    speedup2d=[]
    efficiency2d=[]
    efficiency=[]
    verts   = []
    for Nd in Nds:
        r.append([])
        j = 0
        for fcn in fcns:
            r[i].append([])
            sqlstr = "select threads, totaltime from Results where Nd=" + Nd + " and function='"+fcn+"' and threads <= "+str(thr_max)+" order by ID"
            cur.execute(sqlstr)
            c = cur.fetchall()              #list of tuples 
            non_nes = []
            nested  =[]
            if len(c) > len(fcns):
                non_nes = c[::2]
                nested  = c[1::2]
            else:
                non_nes = c
            ser = min(non_nes, key=lambda x:x[0])[1]
            serial.append(ser) #or nested
            tmp = []
            tmp.append((thr_max,0))
            for n in non_nes:
                tmp.append((n[0], n[1]))
            tmp.append((thr_min,0))
            verts.append(tmp)
            # Make Speedup.
            tmp = []
            tmp.append((thr_max,0))
            for n in non_nes:
                tmp.append((n[0], ser/(n[1]*1.0)))
            tmp.append((thr_min,0))
            speedup.append(tmp)
            tmp = []
            for n in non_nes:
                tmp.append((n[0], ser/(n[1]*1.0)))
            speedup2d.append(tmp)
            #make efficiency.
            tmp = []
            tmp.append((thr_max,0))
            for n in non_nes:
                tmp.append((n[0], (ser/n[1])/(n[0]*1.0)))
            tmp.append((thr_min,0))
            efficiency.append(tmp)                            
            tmp=[]
            for n in non_nes:
                tmp.append((n[0], (ser/n[1])/(n[0]*1.0)))
            efficiency2d.append(tmp)
            tmp = [non_nes, nested]
            r[i][j].append(tmp)
            j+=1
        i+=1
    #verts   = []
    #print(speedup)
    indices = range(thr_min,thr_max+thr_stride, thr_stride)
    colors=[]
    for r in verts:
        colors.append(np.random.rand(3,1))
    print(colors)
    print(efficiency)
    fig  = plt.figure()
    #ax1  = fig.gca(projection='3d')
    ax1 = fig.add_subplot(2,2,1)
    ax2 = fig.add_subplot(2,2,2,projection='3d')
    ax3 = fig.add_subplot(2,2,3,projection='3d')
    ax4 = fig.add_subplot(2,2,4)
    poly = PolyCollection(verts, facecolors=colors)
    poly.set_alpha(0.7)
    for index, n in enumerate(speedup2d):
        fcn = [x[1] for x in n]
        fcn.reverse()
        ax1.plot(range(1,len(fcn)+1), fcn, label="F"+`fcns_i[index]`)
    ax1.legend(bbox_to_anchor=(1.05,1), loc=2, borderaxespad=0.)
    ax1.grid(True)
    ax1.set_xlim(1, thr_max)
    #ax1.set_xticks(range(1,13))
    ax1.set_xlabel("Threads")
    ax1.set_ylabel("Speedup")
    for index, n in enumerate(efficiency2d):
        fcn = [x[1] for x in n]
        fcn.reverse()
        ax4.plot(range(1,len(fcn)+1), fcn, label="F"+`fcns_i[index]`) 
    ax4.legend(bbox_to_anchor=(1.05,1), loc=2, borderaxespad=0.)
    ax4.grid(True)
    ax4.set_xlim(1,thr_max)       
    ax4.set_xlabel("Threads")
    ax4.set_ylabel("Efficiency")
    # ax1.add_collection3d(poly, zs = fcns_i, zdir = 'y')
    # ax1.set_xlabel('Threads')
    # ax1.set_xlim3d(0, thr_max)
    # ax1.set_ylabel('Functions')
    # ax1.set_ylim3d(0,24)
    # ax1.set_zlabel('Time')
    # ax1.set_zlim3d(0, 20)
    poly = PolyCollection(speedup, facecolors=colors)
    poly.set_alpha(0.7)
    ax2.add_collection3d(poly, zs = fcns_i, zdir = 'y')
    ax2.set_xlabel('Threads')
    ax2.set_xlim3d(0, thr_max)
    ax2.set_ylabel('Functions')
    ax2.set_ylim3d(0, 24)
    ax2.set_zlabel('Speedup')
    ax2.set_zlim3d(0, 20)
    poly = PolyCollection(efficiency, facecolors=colors)
    poly.set_alpha(0.7)
    ax3.add_collection3d(poly, zs = fcns_i, zdir = 'y')
    ax3.set_xlabel('Threads')
    ax3.set_xlim3d(0, thr_max)
    ax3.set_ylabel('Functions')
    ax3.set_ylim3d(0,24)
    ax3.set_zlabel('Efficiency')
    ax3.set_zlim3d(0, 1)
    plt.show()


if __name__ == '__main__':
    #read_and_create_graphs('Results/CFO.db', 12) 
    read_and_create_graphs('../CFO_XEON/CFO.db', thr_max = 220, thr_min=10, thr_stride=10)


