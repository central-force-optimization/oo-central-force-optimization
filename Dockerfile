FROM alpine:latest
LABEL maintainer="Rob Green <greenr@bgsu.edu>"

RUN echo "http://dl-4.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories;
RUN echo "http://dl-4.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories;
RUN apk add --update g++ make sqlite-dev bash

VOLUME "/code"

ENTRYPOINT ["bash"]